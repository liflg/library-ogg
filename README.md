Website
=======
https://www.xiph.org/

License
=======
BSD-style license (see the file source/COPYING)

Version
=======
1.3.3

Source
======
libogg-1.3.3.tar.xz (sha256: 4f3fc6178a533d392064f14776b23c397ed4b9f48f5de297aba73b643f955c08)

Required by
===========
* audiere
* libtheora
* libvorbis
* opusfile
* SDL1_mixer
* SDL2_mixer
